/*
 * End Poin of Product
 */
export const HOST_NAME = `https://recruitment.dev.rollingglory.com`;
export const API_VERSION = (url = ``) => `${HOST_NAME}/api/v2${url}`;

export const PRODUCTS = {
  PRODUCT_LIST: (params = '') =>
    API_VERSION(`/gifts${params}`),
  PRODUCT_DETAIL: (id = '') =>
    API_VERSION(`/gifts/${id}`),
  PUT_WISH_LIST: (id = '') =>
    API_VERSION(`/gifts/${id}/wishlist`),
};
