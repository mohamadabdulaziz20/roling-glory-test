// const { fontFamily } = require('tailwindcss/defaultTheme');

module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'smoke': '#D8D8D8',
        'gray':'#838EAB',
        'dark-gray':'#3C3C3F',
        'lime':'#74B71B',
        'lime-light':'#D5E8C6',
        'gray-light':'#E1E1E1',
        'red-lbl': '#E74580'
      },
      // fontFamily: {
      //   sans: ["'Raleway'", ...fontFamily.raleway]
      // },
    },
  },
  variants: {
    extend: {
      backgroundColor: ['checked'],
      borderColor: ['checked'],
    },
  },
  plugins: [],
}
